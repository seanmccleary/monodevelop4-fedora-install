#!/bin/sh

# install the required packages
echo ----------------------------------------------------------
echo Enter the root password to install prereq RMP packages
echo libgnome-devel, libgnomecanvas-devel & libgnomeui-devel
echo ----------------------------------------------------------
su root -c "yum install libgnome-devel libgnomecanvas-devel libgnomeui-devel"

# Make a directory to do the work in
if [ ! -d build ];
then
	mkdir build
fi
cd build

# Download mono.  
curl -L -O http://download.mono-project.com/sources/mono/mono-3.0.7.tar.bz2
tar xf mono-3.0.7.tar.bz2
cd mono-3.0.7
./configure --prefix=/opt/mono-3.0.7 --exec-prefix=/opt/mono-3.0.7
make
echo ----------------------------------------------------------
echo Enter the root password to install mono in /opt/mono-3.0.7
echo ----------------------------------------------------------
su root -c "make install"
cd ..

# Download gtk-sharp; not from mono-project.com's /gtk-sharp directory,
# but from their /gtk-sharp212 directory! (Is that at all confusing?)
# We cannot use 2.12.99, even though it seems to exist and one would assume
# it's the most recent compatible version.  2.12.99 will install
# glib-sharp version 3, and monodevelop will require version 2.

curl -L -O http://download.mono-project.com/sources/gtk-sharp212/gtk-sharp-2.12.11.tar.bz2
tar xf gtk-sharp-2.12.11.tar.bz2
cd gtk-sharp-2.12.11

# These days, files may only include glib.h, however there are still many
# lingering files which try to include other files directly.
# Apparently this is easy to fix by just making the breaking files
# include glib.h. It's discussed here: 
# http://www.linuxquestions.org/questions/linux-software-2/repeated-build-error-913859/
sed -i 's/<glib\/gthread.h>/<glib.h>/' glib/glue/thread.c

MONO_PATH=/opt/mono-3.0.7 PATH=/opt/mono-3.0.7/bin:$PATH PKG_CONFIG_PATH=/opt/mono-3.0.7/lib/pkgconfig ./configure --prefix=/opt/mono-3.0.7/
make
echo --------------------------------------------------------------------
echo Enter the root password to install mono gtk-sharp in /opt/mono-3.0.7
echo --------------------------------------------------------------------
su root -c "make install"
cd ..

# Download gnome-sharp; get it from mono-project.com's /gnome-sharp2,
# directory, because it contains version 2.24.1 while their
# /gnome-sharp220 directory only contains up to 2.20.1!
curl -L -O  http://download.mono-project.com/sources/gnome-sharp2/gnome-sharp-2.24.1.tar.bz2
tar xf gnome-sharp-2.24.1.tar.bz2
cd gnome-sharp-2.24.1
MONO_PATH=/opt/mono-3.0.7 PATH=/opt/mono-3.0.7/bin:$PATH PKG_CONFIG_PATH=/opt/mono-3.0.7/lib/pkgconfig ./configure --prefix=/opt/mono-3.0.7/
# There's some broken stuff here, too.  A few lines need to be removed from
# a Makefile. You can read about it here:
# http://mono.1490590.n4.nabble.com/Mono-2-8-gnome-sharp-not-compiling-td2969455.html
# Where Miguel says "We no longer ship Mono.GetOptions.dll ... we should get the
# sample rewritten" in October of 2010.
sed -i '449s/^/#/' sample/gnomevfs/Makefile
sed -i '450s/^/#/' sample/gnomevfs/Makefile
sed -i '221s/^/#/' sample/gnomevfs/Makefile
make
echo ----------------------------------------------------------------------
echo Enter the root password to install mono gnome-sharp in /opt/mono-3.0.7
echo ----------------------------------------------------------------------
su root -c "make install"
cd ..

# Download libgdiplus
curl -L -O http://download.mono-project.com/sources/libgdiplus/libgdiplus-2.10.tar.bz2
tar xf libgdiplus-2.10.tar.bz2
cd libgdiplus-2.10
# As with just about every mono lib, there's a problem with this one. See:
# http://stackoverflow.com/questions/8823834/having-trouble-installing-libgdiplus
# It has a link to a patch which we will download and apply
curl -o "libgdiplus-2.10.1-libpng15.patch" "http://svnweb.mageia.org/packages/cauldron/libgdiplus/current/SOURCES/libgdiplus-2.10.1-libpng15.patch?revision=142282&view=co&pathrev=142282"
patch -p0 < libgdiplus-2.10.1-libpng15.patch
MONO_PATH=/opt/mono-3.0.7 PATH=/opt/mono-3.0.7/bin:$PATH PKG_CONFIG_PATH=/opt/mono-3.0.7/lib/pkgconfig ./configure --prefix=/opt/mono-3.0.7/
make
echo ----------------------------------------------------------------
echo Enter the root password to install libgdiplus in /opt/mono-3.0.7
echo ----------------------------------------------------------------
su root -c "make install"
cd ..

# Download and install monodevelop
# Don't get the latest version from their site (which first says 3 then
# later says 4, and if you download for Mac says "Xamarin Studion" 
# instead of MonoDevelop (but if this Linux version builds it will
# continuet to say MonoDevelop) ... the version available on their
# site is missing a ton of project Templates.
# Is there a newer version on their github?
# Why yes there is, it looks like there's a 4.0.8 as of the
# time of this writing.
git clone --recursive https://github.com/mono/monodevelop.git
cd monodevelop
git checkout monodevelop-4.0.8
MONO_PATH=/opt/mono-3.0.7 PATH=/opt/mono-3.0.7/bin:$PATH PKG_CONFIG_PATH=/opt/mono-3.0.7/lib/pkgconfig ./configure --prefix=/opt/mono-3.0.7/ --profile=stable
make
echo -----------------------------------------------------------------
echo Enter the root password to install monodevelop in /opt/mono-3.0.7
echo -----------------------------------------------------------------
su root -c "make install"

echo -----------------------------------------------------------------
echo OK, you can copy the monodevelop script in this same directory
echo to /usr/local/bin.
echo There is an included .desktop file you can put into 
echo ~/.local/share/applications/
echo -----------------------------------------------------------------
